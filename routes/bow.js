var express = require('express');
var axios = require('axios');
var htmlToText = require('html-to-text');
var router = express.Router();

var tm = require('textmining');
var read = require('node-readability');

/* GET users listing. */
router.post('/', async function(req, res, next) {

let {content: text, title} = await new Promise((resolve, reject) => {

  read(req.body.theUrl, function(err, article, meta) {

    if (err) return reject(err)    

    resolve({content: article.textBody, title: article.title})
  
    // Close article to clean up jsdom and prevent leaks
    article.close();
  }) 
})

text = title + "\n" + text;
    
let bag = tm.bagOfWords( text.split("\n"), true, true );

  var parsedpage = bag.terms.sort(function(a,b){
    if( a.frequency > b.frequency ) 		return -1;
    else if( a.frequency < b.frequency ) 	return 1;
    else return 0;
  }).filter((n) => n.term);

  let bagofwords = parsedpage.slice(0, 10)

  console.log(bagofwords)

  res.render('bow', {bagofwords});
});

module.exports = router;
